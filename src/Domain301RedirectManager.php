<?php

namespace Drupal\domain_301_redirect;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PrivateKey;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines an Domain301RedirectManager service.
 */
class Domain301RedirectManager implements Domain301RedirectManagerInterface {

  /**
   * A config object for the Domain 301 Redirect configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The Drupal site private key.
   *
   * @var \Drupal\Core\PrivateKey
   */
  protected $privateKey;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Constructs an Domain301RedirectManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Configuration Factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\PrivateKey $private_key
   *   The site private key.
   * @param \GuzzleHttp\ClientInterface $client
   *   The http client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack, PrivateKey $private_key, ClientInterface $client) {
    $this->config = $config_factory->get('domain_301_redirect.settings');
    $this->request = $request_stack->getCurrentRequest();
    $this->privateKey = $private_key;
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function checkDomain($domain) {
    // Try to contact the redirect domain, if this fails, retry 3 times after
    // a pause.
    $uri = $this->getRedirectCheckUrl($domain)->toString();
    for ($i = 1; $i <= 3; $i++) {
      try {
        $response = $this->client->get($uri);
        if ($response->getStatusCode() == 200) {
          return TRUE;
        }
      }
      catch (RequestException $e) {
      }

      // Pause between retries.
      sleep(5);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectCheckUrl($domain) {
    $url = Url::fromRoute('domain_301_redirect.check', [], [
      'query' => ['token' => $this->getRedirectCheckToken()],
      'base_url' => $domain,
      'absolute' => TRUE,
    ]);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectCheckToken() {
    return Crypt::hmacBase64('domain_301_redirect_check_domain', Settings::getHashSalt() . $this->privateKey->get());
  }

}
