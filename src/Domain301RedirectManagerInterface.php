<?php

namespace Drupal\domain_301_redirect;

/**
 * Interface for Domain301RedirectManager.
 */
interface Domain301RedirectManagerInterface {

  /**
   * Define if configured paths should be included or excluded from redirection.
   */
  public const EXCLUDE_METHOD = 0;

  public const INCLUDE_METHOD = 1;

  /**
   * Checks if a domain actually points to this site.
   *
   * @param string $domain
   *   The domain to be checked.
   *
   * @return bool
   *   Returns TRUE if the domain passes the check. FALSE otherwise.
   */
  public function checkDomain($domain);

  /**
   * Generate a url to the redirect check controller endpoint.
   *
   * @return \Drupal\Core\Url
   *   The redirect check url.
   */
  public function getRedirectCheckUrl($domain);

  /**
   * Generate a token based on redirect_last_checked.
   *
   * @return string
   *   Returns the token.
   */
  public function getRedirectCheckToken();

}
