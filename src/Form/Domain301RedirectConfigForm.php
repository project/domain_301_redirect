<?php

namespace Drupal\domain_301_redirect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\domain_301_redirect\Domain301RedirectManager;
use Drupal\domain_301_redirect\Domain301RedirectManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a settings for domain_301_redirect module.
 */
class Domain301RedirectConfigForm extends ConfigFormBase {

  /**
   * Domain 301 Redirect Manager.
   *
   * @var \Drupal\domain_301_redirect\Domain301RedirectManager
   */
  protected $redirectManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\domain_301_redirect\Domain301RedirectManager $domain_301_redirect_manager
   *   The Domain301RedirectManager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Domain301RedirectManager $domain_301_redirect_manager, DateFormatterInterface $date_formatter) {
    parent::__construct($config_factory);
    $this->redirectManager = $domain_301_redirect_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('domain_301_redirect.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_301_redirect_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['domain_301_redirect.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('domain_301_redirect.settings');

    $form['enabled'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#description' => $this->t('Enable this setting to start 301 redirects to the domain below for all other domains.'),
      '#options' => [
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
      '#default_value' => $config->get('enabled'),
    ];

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#description' => $this->t('This is the domain that all other domains pointing to this site will be 301 redirected to. This value should also include the scheme such as http or https but will redirect to http if not specified. Example: http://www.testsite.com'),
      '#default_value' => $config->get('domain'),
    ];

    // Per path configuration settings to apply the redirect to specific paths.
    $form['applicability'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Pages'),
      '#open' => TRUE,
    ];

    $form['applicability']['pages'] = [
      '#type' => 'textarea',
      '#default_value' => $config->get('pages'),
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
        '%blog' => '/blog',
        '%blog-wildcard' => '/blog/*',
        '%front' => '<front>',
      ]),
    ];

    $form['applicability']['applicability'] = [
      '#type' => 'radios',
      '#options' => [
        Domain301RedirectManagerInterface::EXCLUDE_METHOD => $this->t('Do not redirect for the listed pages'),
        Domain301RedirectManagerInterface::INCLUDE_METHOD => $this->t('Only redirect for the listed pages'),
      ],
      '#default_value' => $config->get('applicability'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!$form_state->getValue('enabled')) {
      return;
    }
    // Clean & trim the domain.
    $domain = trim(trim($form_state->getValue('domain')), '/');

    if (!preg_match('|^https?://|', $domain)) {
      $domain = 'http://' . $domain;
    }

    if (!UrlHelper::isValid($domain, TRUE)) {
      $form_state->setErrorByName('domain', $this->t('Domain 301 redirection can not be enabled if a valid domain is not set.'));
      return;
    }

    $domain_parts = parse_url($domain);
    $domain = $domain_parts['scheme'] . '://' . $domain_parts['host'] . (!empty($domain_parts['port']) ? ':' . $domain_parts['port'] : '');
    $form_state->setValue('domain', $domain);
    if (!$this->redirectManager->checkDomain($domain)) {
      $form_state->setErrorByName('enabled', $this->t('Domain 301 redirection can not be enabled as the domain you set does not currently point to this site.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $this->config('domain_301_redirect.settings')
      ->set('enabled', $values['enabled'])
      ->set('domain', $values['domain'])
      ->set('applicability', $values['applicability'])
      ->set('pages', $values['pages'])
      ->save();
  }

}
